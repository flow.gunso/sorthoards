# ToDo list

All those tasks are required for v1.

### High priority
- support python >2.7
- support utf8 filenames

### Low priority
- add a debug mode
- add a logger
- add statistics
- set proper exit codes