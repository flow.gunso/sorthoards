def main():
    import cli
    import hoards
    import time
    import shutil
    import os
    import sys
    
    # Restrict to Python 2.7.
    
    if sys.version_info.major != 2 and sys.version_info.minor != 7:
        sys.exit('Sorry, only Python 2.7 is supported')

    args = cli.parse()
    cli.validate(args)
    
    source = args.source
    destination = args.destination

    # Initiate the statistics path and file.
    statistics_path=".stats/"
    statistics_file="statistics_"+str(int(time.time()))+".json"
    statistics = os.path.join(statistics_path, statistics_file)
    if os.path.exists(statistics_path) is False:
        os.mkdir(statistics_path)
        
    # Initiate timer for hash computation performance comparisons.
    timer = time.time()
    
    # Loop over all files and directories from the source.
    for root, dirs, files in os.walk(source):

        for filename in files:
#            print(filename+":")

            # Get the file's extension.
            extension = hoards.get_extension(filename)
#            print("\t"+extension)

            # Build the destination path.
            destination_path = os.path.join(destination, extension)
#            print("\t"+destination_path)
            if os.path.exists(destination_path) is False:
                os.mkdir(destination_path)

            # Set full source and destination file's path.
            source_file = os.path.join(root, filename)
            destination_file = os.path.join(destination_path, filename)
#            print("\t"+source_file)
#            print("\t"+destination_file)

            # Delete if there's a collision, increment and move otherwise.
            if os.path.exists(destination_file) is True:
                if hoards.check_collision(source_file, destination_file, args.quick) is True:
                    os.remove(source_file)
                else:
                    destination_file = hoards.increment_filename(destination_file)
                    os.rename(source_file, destination_file)

            # Move the file if no collision.
            else:
                # And move the file.
                os.rename(source_file, destination_file)

    # Cleanup the source.
    hoards.cleanup(source)

    print("Script took " + str(time.time()-timer) + "s.")

if __name__ == '__main__':
    main()
