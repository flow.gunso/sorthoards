import os
import hashlib
import glob
import sys
import json
import time
import zlib

def get_extension(filename):
    """ Return a string
    
    Extract the extension from a filename
    """
    
    # An extension is found, extract and lowercase the last extension.
    if filename.find('.') != -1:
        extension = filename.split('.')[-1].lower()
        #print(f + " has " + destDir + " extension. Send to " + destDir)

    # No extension is found, set a whitespace as the file extension.
    else:
        extension = " "
        #print(f + " has not extension. Send to " + destDir)

    return(extension)

def cleanup(source, level=0):
    """
    Recursively remove all the folders in the source. Source should not contain any files.
    """

    # Retrieve source content.
    source_list = os.listdir(source)
    
    # Loop over the source content if some has been found.
    if source_list is not None:
        for item in os.listdir(source):
        
            # Exit the programm, because a file has been found. It shouldn't have.
            if os.path.isdir(source+"/"+item) is False:
                print("Something was found in the directory "+os.path.isfile(path)+"/ while cleanup. Exiting")
                sys.exit(3)
            
            # Recursively cleanup the directory.
            else:
                cleanup(source+"/"+item, level=level+1)

    # Remove the source as it is now empty, but the source root.
    if level > 0:
        os.rmdir(source)

def compute_crc32_chunks(file):
    """ return a checksum
    
    Checksum the file as chunks with CRC32.
    """
    BUF_SIZE = 65536
    csum = 0

    # Compute the chunks hashes.
    with open(file, 'rb') as f:
        while True:
            chunk = f.read(BUF_SIZE)
            if not chunk:
                break
            csum = zlib.crc32(chunk, csum)

    return(csum & 0xffffffff)

def compute_crc32(file):
    """ return a checksum
    
    Checksum the file with CRC32.
    """

    # Compute the chunks hashes.
    buf = open(file, 'rb').read()
    csum = zlib.crc32(buf)

    return(csum & 0xffffffff)


def compute_sha1(file):
    """ return a string
    
    Compute a sha1 hex digest for a file.
    Methology is based upon http://ow.ly/8xDs307G3nn @ranman.
    """

    # Initialize SHA1 hashing.
    BUF_SIZE = 65536  # lets read stuff in 64kb chunks!
    sha1 = hashlib.sha1()

    # Compute the source file hash by chunks.
    with open(file, 'rb') as f:
        while True:
            data = f.read(BUF_SIZE)
            if not data:
                break
            sha1.update(data)

    return(sha1.hexdigest())

def check_collision(source, destination, quick):
    """ return boolean
    
    Check whether a duplicate file already exist in the destination.
    Because of the program's purpose, comparing the file size first
    will filter almost all duplicates, fast. Then in case of false
    duplicates, two checksum are computed. This prevent false positives.
    """

    # Compare file sizes first.
    source_size = os.path.getsize(source)
    destination_size = os.path.getsize(destination)
    
    # Compare the file sizes:
    if source_size == destination_size:

        # Quick sorting enabled
        if quick is True:
            status = True

        # Quick sorting disabled: compute checksums.
        else:
            # Compute CRC32 checksum first.
            source_crc32 = compute_crc32_chunks(source)
            destination_crc32 = compute_crc32_chunks(destination)
            if source_crc32 == destination_crc32:
                
                # Finally compute the SHA1 hash to prevent false positives.
                source_sha1 = compute_sha1(source)
                destination_sha1 = compute_sha1(destination)
                if source_sha1 == destination_sha1:
                    status = True
                else:
                    status = False
            else:
                status =  False
    else:
        status = False

    return(status)

def increment_filename(destination):
    """ Return a string
    
    Safely increment a filename to solve collision issues.
    """

    # Filter all matching files to the destination.
    collided_files = glob.glob(destination+"*")
    # Compute the next increment for the destination file.
    n_collided_files=len(collided_files)
    n_collided_files += 1
    # Return the correct filename.
    return(destination+"."+str(n_collided_files))
