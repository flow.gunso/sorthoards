import argparse
import sys
import os
from distutils.util import strtobool

# from http://stackoverflow.com/a/22222073
def user_yes_no_query(question):
    sys.stdout.write('%s [y/n]\n' % question)
    while True:
        try:
            return strtobool(raw_input().lower())
        except ValueError:
            sys.stdout.write('Please respond with \'y\' or \'n\'.\n')

def parse():
    """
    Option parser
    """
    # Parser definition.
    parser=argparse.ArgumentParser(description="Sort files from a source into a destination using their extension's.")
    parser.add_argument('source', help="The source for the files.")
    parser.add_argument('destination', help="The destination for the files.")
    parser.add_argument('-f', '--force', dest='force', action='store_true', help="Disable interactive mode.")
    parser.add_argument('-q', '--quick-sort', dest='quick', action='store_true', help="Disable file checksums for quick sorting. May result in data loss.")
#    parser.add_argument('-l', '--log-level', dest='log', type=int, help="Specify source from where gathering files")
#    parser.add_argument('-v', '--version', action='version', version='')

    # Print help if arguments parser fails.
    try:
        args = parser.parse_args()
        return(args)
    except:
        parser.print_help()
        sys.exit(1)

def validate(args):
    # Check the source and destination validity.
    if not os.path.isdir(args.source) and not os.path.isdir(args.destination):
        print(sys.argv[0] + ": error: `" + args.source + "` and `" + args.destination + "` are not directories.")
        sys.exit(2)
    elif not os.path.isdir(args.destination):
        print(sys.argv[0] + ": error: `" + args.destination + "` is not a directory")
        sys.exit(2)
    elif not os.path.isdir(args.source):
        print(sys.argv[0] + ": error: `" + args.source + "` is not a directory")
        sys.exit(2)

    # Remove ending slash if ther's some.
    if args.source[-1] == '/': args.source = args.source[:-1]
    if args.destination[-1] == '/': args.destination = args.destination[:-1]

    # Prevent the destination being within the source.
    if args.source in args.destination and len(args.destination) >= len(args.source): 
        print(sys.argv[0] + ": error: the destination can't be within the source.")
        sys.exit(2)

    # Check if the source isn't empty.
    if not os.listdir(args.source):
        print(sys.argv[0] + ": error: `" + args.source + "` is empty.")
        sys.exit(2)

    # Interactive for user validation.
    if not args.force:
        print("Use " + args.source + "/ as source and " + args.destination + "/ as destination for sorting ? ")
        if not user_yes_no_query("Are you sure ? "):
            print("Bye then !")
            sys.exit(2)