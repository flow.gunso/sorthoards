Have you ever found yourself clogged by the amount of files in a folder ? So many many files you can't manually sort them for the next 3 hours ?

SortHoards is having a go at this issue.
It will sort all the files from a source directory to a destination directory.
In the destination, files will be moved into directories matching their extensions.

SortHoard is currently restricted to Python 2.7. 

# Features

- support any files extension, even the files that don't have any
- delete duplicate files
- keep non-duplicate files with matching names

# Improvement

see [TODO](https://github.com/flowgunso/SortHoards/blob/master/docs/TODO.md)

# Known bugs

see [KNOWN_BUGS](https://github.com/flowgunso/SortHoards/blob/master/docs/KNOWN_BUGS.md)

# Contribution

You are always welcome to contribute.
This project is licence under MIT, so yeah, go ahead.
